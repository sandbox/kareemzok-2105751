DESCRIPTION
-----------
This module integrate the wonderful jquery calendar plugin built by Jaime Fdez:
http://www.vissit.com/projects/eventCalendar/

You can create schedule any content this calendar.

REQUIREMENTS
------------
Drupal 7.x

This module requires the following contrib module :
 *  Date (https://www.drupal.org/project/date)
 *  libraries (https://www.drupal.org/project/libraries)


INSTALLATION
------------
 *  Install as you would normally install a contributed drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.
 *  Download the Jquery event calendar library from https://github.com/jaime8111/jQueryEventCalendar
 *  Place the library in the appropriate directory E.G.
    sites/all/libraries/jquery-event-calendar
    sites/all/libraries/jquery-event-calendar/eventCalendar.css
    sites/all/libraries/jquery-event-calendar/eventCalendar_theme_responsive.css
    sites/all/libraries/jquery-event-calendar/jquery.jquery_event_calendar.js


CONFIGURATION
-------------
 *  Jquery event calendar settings can be found at admin/config/user-interface/jquery-event-calendar.
 *  Settings are defined as groups.
 *  Once you have configured your settings; You will be able to see the result on the calendar.
 *  Finally be sure you have selected the date field in settings and entered the date in content.

